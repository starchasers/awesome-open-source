# Awesome Open Source

List of nice open source projects.
  
This list ais created by starchasers group and they add what they want.
Maybe you can find something useful in here.

## Operating System
  
### Linux distro
  
+ [Linux Mint](https://linuxmint.com/): An operating system designed for newbie
+ [NixOS](https://nixos.org/): An advanced operating system for DevOps
  
### BSD
  
+ [OpenBSD](https://www.openbsd.org/): An operating focus on security and correctness
  
## Software
  
### Terminal emulators
  
+ [alacritty](https://alacritty.org/): A terminal is written in Rust

  
## Drivers
  
+ [capdriver](https://github.com/mounaiban/captdriver): an alternative driver for Canon laser printers
